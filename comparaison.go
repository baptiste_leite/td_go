package main

import (
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"log"
)

const PIC1 = "images/image_1.jpg"
const PIC2 = "images/image_2.jpg"
const PIC3 = "images/image_3.jpg"

func decoupImage(pic string) int {
	content, err := ioutil.ReadFile(pic)
	if err != nil {
		log.Fatal(err)
	}
	h := sha256.New()
	write, err := h.Write(content)
	return write
}

func main() {
	var im1 = decoupImage(PIC1)
	var im2 = decoupImage(PIC2)
	var im3 = decoupImage(PIC3)

	if im1 != im2 && im1 != im3 {
		fmt.Println(PIC1)
	} elif im1 != im2 && im3 != im2 {
		fmt.Println(PIC2)
	} else {
		fmt.Println(PIC3)
	}
}